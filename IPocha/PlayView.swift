//
//  PlayView.swift
//  IPocha
//
//  Created by Enrique Vicent on 09/11/2020.
//

import SwiftUI

struct PlayView: View {
    @EnvironmentObject var partida: Partida
    
    var body: some View {
        VStack(){
            PartidaHeadView()
            List(){
                ForEach( partida.points ){ roundIndex in
                    RondaView(ronda: roundIndex.id)
                }
            }
            Button(action: {
                _ = partida.appendRound()
            }, label: {
                HStack {
                    Text("Añadir ronda")
                    Image(systemName: "plus.circle.fill")
                }
            }).padding()
            Spacer()
        }
    }
}

struct PlayView_Previews: PreviewProvider {
            
    static var previews: some View {
        Group {
            PlayView().environmentObject(Partida.preview_4Players4Rounds()).environmentObject(Settings())
            PlayView().environmentObject(Partida()).environmentObject(Settings())
        }
    }
}


