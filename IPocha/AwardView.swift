//
//  AwardView.swift
//  IPocha
//
//  Created by Enrique Vicent on 7/12/20.
//

import SwiftUI

struct AwardView: View {
    let award: Award
    var body: some View {

        VStack{
            HStack {
                Spacer()
                Text(award.name)
                Spacer()
            }
            Text(award.winner.name.capitalized)
                .font(.title2)
                .padding(.vertical)
            if let score = award.amount {
                HStack{
                    Text("\(score)")
                        .bold()
                        .foregroundColor(.accentColor)
                    if let units = award.unit{
                        Text(units)
                            .font(.footnote)
                    }
                }
            }
            if let reasons = award.reason {
                Text(reasons).italic()
            } else {
                Text(" ")
            }
            Spacer()
        }
        .padding(.vertical)
        .background(Color.primary.colorInvert())
        .cornerRadius(18)
        .padding(2.0)
        .background(Color.blue)
        .cornerRadius(20)
    }
}

struct AwardView_Previews: PreviewProvider {
    static var previews: some View {
        AwardView(award: Award(name: "Ganador", winner: Player(id: UUID(), name: "pedro"), amount: 120, unit: "puntos", reason: nil, Description: "ganador al final de la ronda"))
    }
}
