//
//  PartidaStatistics.swift
//  IPocha
//
//  Created by Enrique Vicent on 4/12/20.
//

import Foundation


struct Award {
    let id = UUID()
    let name: String
    let winner: Player
    let amount: Int?
    let unit: String?
    let reason: String?
    let Description: String?
}

extension Partida {
    func searchPayer(playerUUID: UUID?) -> Player? {
        var result: Player? = nil;
        if let playerUUID_ = playerUUID {
            if let playerIndex = self.playerIndex(playerUUID: playerUUID_) {
                result = self.players[playerIndex]
            }
        }
        return result
    }
    
    fileprivate func playerStadisticsByPoints (awardName : String, awardDescription: String, points: [UUID : Int], _ comp: ((key: UUID, value: Int),(key:UUID, value:Int)) -> Bool) -> Award? {
        let maxPlayer = points.max(by: comp)
        let maxPoits = maxPlayer?.value
        if let player : Player = self.searchPayer(playerUUID: maxPlayer?.key){
            return Award(name: awardName, winner: player, amount: maxPoits , unit: "puntos", reason: nil, Description: awardDescription)
        } else {
            return nil
        }
        
    }
    
    fileprivate var fullTablePoints: [UUID : Int]? {
        let maxRound: Int = self.gameSpan.current - 1
        if maxRound < 1 {
            return nil
        } else {
            return self.sumaryPoints(upToRound: maxRound)
        }
    }
    
    fileprivate enum top {
        case winner
        case looser
    }
    
    fileprivate func topPlayerFor (
        to:top,
        pointsTable: [UUID : Int]?,
        awardName: String,
        awardDescription: String
    ) -> Award? {
        let sortCriteria: ((key: UUID, value: Int),(key:UUID, value:Int)) -> Bool
        switch to {
        case .winner:
            sortCriteria =  { (a,b) -> Bool in a.value < b.value}
        case .looser:
            sortCriteria = { (a,b) -> Bool in a.value > b.value}
        }

        if let points = pointsTable {
            return playerStadisticsByPoints(awardName: awardName, awardDescription: awardDescription, points: points, sortCriteria)
        } else {
            return nil
        }
    }
    
    var playerWinner: Award? {
        topPlayerFor(to: .winner, pointsTable: self.fullTablePoints, awardName: "ganador", awardDescription: "ganador actual")
    }
    
    var playerLoser: Award? {
        topPlayerFor(to: .looser, pointsTable: self.fullTablePoints, awardName: "perdedor", awardDescription: "perdedor actual")
    }
    
    var playerWinnerUpHill: Award? {
        let rondaFinSubida = self.rondaTodasInicio - 1
        if currentRound < rondaFinSubida {
            return nil
        } else {
            let puntos = self.partialPoints(toRound: rondaFinSubida)
            return topPlayerFor(to: .winner, pointsTable: puntos, awardName: "escalador", awardDescription: "ganador durante la subida")
        }
    }
    
    var playerWinnerDownHill: Award? {
        if currentRound < rondaBajada  {
            return nil
        } else {
            let puntos = self.partialPoints(fromRound: rondaBajada, toRound: min( self.currentRound, rondafinBajada))
            return topPlayerFor(to: .winner, pointsTable: puntos, awardName: "bajador", awardDescription: "ganador durante la bajada")
        }
    }
    
    fileprivate func add2Partials (_ pointsTableA: [UUID : Int]?, _ pointsTableB: [UUID : Int]?) -> [UUID : Int]? {
        if let b = pointsTableB {
            if let a = pointsTableA {
                return a.merging(b, uniquingKeysWith: {$0+$1})
            } else {
                return pointsTableB
            }
        } else {
            return pointsTableA
        }
    }
    
    var playerWinnerAllCards: Award? {
        if currentRound < rondaTodasInicio {
            return nil
        } else {
            let awardName = "de '\(self.maxCartasPerRound)'s"
            let awardDescription = "ganador en rondas de todas las cartas"
            let rondaTodasInicio = self.rondaTodasInicio
            let currentRound = self.currentRound
            let rondaBajada = self.rondaBajada
            let rondafinBajada = self.rondafinBajada
            let puntosPrimerasTodas = self.partialPoints(fromRound: rondaTodasInicio, toRound: min(currentRound, rondaBajada - 1))
            let todosPuntosTodas: [UUID : Int]?
            if rondafinBajada+1 > currentRound {
                todosPuntosTodas = puntosPrimerasTodas
            } else {
                let puntosSegundasTodas = self.partialPoints(fromRound: rondafinBajada+1, toRound: currentRound)
                todosPuntosTodas = add2Partials(puntosPrimerasTodas, puntosSegundasTodas)
            }
            return topPlayerFor(to: .winner, pointsTable: todosPuntosTodas, awardName: awardName, awardDescription: awardDescription)
        }
    }
}
