//
//  Partida.swift
//  IPocha
//
//  Created by Enrique Vicent on 27/10/2020.
//

import Foundation

enum Rules {
    case pocha
    case pocheada
}

enum ModificadoresRonda {
    case doble
    case triple
    case simple
}



struct Player: Identifiable {
    var id = UUID()
    var name: String
}

class Partida: ObservableObject {
    @Published var rules: Rules = .pocha
    @Published var currentRound = 0
    @Published var players: [Player] = []
    @Published var points: [Ronda] = []
    
    //var puntos: Ronda { Ronda(modificadorRonda: .simple, puntos: [:])}
    var numberOfPlayers:Int { players.count}
    var isActive: Bool {!points.isEmpty}
    var baraja: Int  {
        switch numberOfPlayers {
        case 0:
            return 40
        case 3:
            return 27 //para hacer la partida razonablemnte corta
        default:
            return (40 / numberOfPlayers) * numberOfPlayers
        }
    }
    /// numero de rodas de la partida
    /// - returns:
    ///   - current: the number or current created rounds
    ///   - expected: the number of expected rounds excluding final draw rounds
    /// - note: if draw on final round the *current* value is bigger than *expected*
    var gameSpan: (current : Int, planned : Int) {
        let expectedSpanForPocha = numberOfPlayers == 0 ? 0 : ((baraja / numberOfPlayers) - 1 ) * 2 + numberOfPlayers
        let expectedSpanForPocheada = expectedSpanForPocha + numberOfPlayers*2 - 2 //quitamos 2, la primera de 1 que ocurre igual y la ultima de 1 bajando que no se juega
        let expectedSpan = self.rules == .pocha ? expectedSpanForPocha : expectedSpanForPocheada
        let currentSpan = self.points.count
        return (current: currentSpan, planned: expectedSpan )
    }
    /// numero de cartas que se dan en la ronda de 'todas'
    var maxCartasPerRound: Int {numberOfPlayers == 0 ? baraja : baraja / numberOfPlayers}
    /// numero de rondas dando una al principio
    var rondasRepitiendoUna: Int { self.rules == .pocha ? 1 : numberOfPlayers }
    /// numero de ronda  donde se dan 2 cartas
    var ronda2CartasSubiendo: Int {rondasRepitiendoUna}
    /// numero de rondas donde se empiezan a repartir todas
    /// - note: empezando a contar desde 0
    var rondaTodasInicio:Int {ronda2CartasSubiendo - 2 + maxCartasPerRound}
    /// número primera ronda de  la bajada
    var rondaBajada: Int {rondaTodasInicio + numberOfPlayers}
    /// ronda final de pocha o primera ronda de todas en pocheada
    var rondafinal: Int {rondaBajada + maxCartasPerRound - 2}
    /// ultima ronda del descenso
    var rondafinBajada: Int {rules == Rules.pocha ? rondafinal : rondafinal - 1}
}

extension Partida {
    func addPlayer (name: String) -> Partida {
        let result = self
        result.players.append(Player(name: name))
        return result
    }
    func removePlayer (index: Int) -> Partida {
        let result = self
        result.players.remove(at: index)
        return result
    }
    func appendRound() -> Ronda {
        let playerIDs: [UUID] = players.map { $0.id}
        let nextRoundNumber = self.points.count
        let newRonda = Ronda(players: playerIDs, roundNumber: nextRoundNumber)
        self.currentRound = nextRoundNumber
        self.points.append(newRonda)
        return newRonda
    }
    func sumaryPoints(upToRound till:Int) -> [UUID:Int]{
        partialPoints(fromRound:0, toRound: till)
    }
    
    func partialPoints(fromRound from:Int=0, toRound till:Int) -> [UUID:Int]{
        var result : [UUID:Int] = [:]
        if till >= points.count {
            for player in self.players {
                result[player.id]=0
            }
            return result
        }
        let rondas = points[from...till]
        result = rondas.reduce (result) { partial, ronda in
            var partialCopy = partial
            for (key,values) in ronda.sumario {
                if let currentPoints = partialCopy [key] {
                    partialCopy[key] =  currentPoints + values.puntos
                } else {
                    partialCopy[key] = values.puntos
                }
            }
            return partialCopy
        }
        return result
    }
    
    func cartas(ronda rondaIndex: Int) -> Int {
        if(numberOfPlayers == 0) {
            return 0
        }
        let max = baraja / numberOfPlayers
        switch rondaIndex {
        case 0 ... rondasRepitiendoUna - 1: //iniciales
            return 1
        case ronda2CartasSubiendo ..< rondaTodasInicio: //subiendo
            return rondaIndex - ronda2CartasSubiendo + 2
        case rondaTodasInicio ..< rondaBajada : //medio
            return max
        case  rondaBajada ..< rondafinal :
            let rondasDesdeElTope = rondaIndex - rondaBajada //bajando
            return max - 1 - rondasDesdeElTope
        case rondafinal: //ronda final de pocha o primera ronda de todas en pocheada
            if self.rules == .pocha {
                return 1
            } else {
                return max
            }
        case rondafinal + 1 ..< rondafinal + numberOfPlayers : //resto de rondas de todas de pocheada
            if self.rules == .pocha {
                return 0
            } else {
                return max
            }
        default: //partida terminada
            return 0
        }
    }
    
    func findRonda (identity : Int) -> Ronda {
        return self.points[identity]
    }
    
    func clear () {
        rules = .pocha
        currentRound = 0
        players = []
        points = []
    }
}


