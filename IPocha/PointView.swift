//
//  PointView.swift
//  IPocha
//
//  Created by Enrique Vicent on 18/11/20.
//

import SwiftUI

struct PointView: View {
    @EnvironmentObject var partida: Partida
    let playerId: UUID
    let viewMode: PreferedDataView
    let ronda: Int
    
    fileprivate func formatPointView(point x: Int?) -> Text {
        if let point = x {
            return point < 0  ? Text("\(point)").foregroundColor(.red) : Text("\(point)")
        }
        else {
            return Text(" ¿? ")
        }
    }
    
    var body: some View {
        switch viewMode {
        case .puntos:
            let points = partida.findRonda(identity: ronda).sumario[playerId]?.puntos
            return formatPointView(point: points)
        case .acum:
            let acum = self.partida.sumaryPoints(upToRound: ronda)[playerId]
            return formatPointView(point: acum)
        case .puntosAndAcum:
            let puntosString : String
            let acumString : String
            if let puntos = partida.findRonda(identity: ronda).sumario[playerId] {
                puntosString = puntos.puntos < 0 ? "\(puntos.puntos)" : "+\(puntos.puntos)"
            } else {
                puntosString = "x?"
            }
            if let acum = self.partida.sumaryPoints(upToRound: ronda )[playerId]{
                acumString = "\(acum)"
            } else {
                acumString = ""
            }
            return Text("\(puntosString) = \(acumString)")
        case .aciertos:
            let aciertos = partida.findRonda(identity: ronda).aciertos[playerId]
            return formatPointView(point: aciertos)
        }
    }
}

struct PointView_Previews: PreviewProvider {
    
    static func createSettings (peferedView : PreferedDataView) -> Settings {
        let result = Settings()
        result.preferredData = peferedView
        return result
    }
    static let partidaPreview = Partida.preview_4Players4Rounds()
    static let settingPreviewAciertos = createSettings(peferedView: .aciertos)
    
    static var previews: some View {
        Group {
            PointView(playerId: partidaPreview.players[0].id , viewMode: PreferedDataView.aciertos, ronda: 2)
                .environmentObject(partidaPreview)
            PointView(playerId: partidaPreview.players[0].id , viewMode: PreferedDataView.puntos, ronda: 2)
                .environmentObject(partidaPreview)
            PointView(playerId: partidaPreview.players[0].id , viewMode: PreferedDataView.puntosAndAcum, ronda: 2)
                .environmentObject(partidaPreview)
            PointView(playerId: partidaPreview.players[0].id , viewMode: PreferedDataView.acum, ronda: 2)
                .environmentObject(partidaPreview)
        }
    }
}
