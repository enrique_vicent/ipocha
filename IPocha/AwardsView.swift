//
//  AwardsView.swift
//  IPocha
//
//  Created by Enrique Vicent on 7/12/20.
//

import SwiftUI


struct AwardsView: View {
    
    private let cteSpacing = CGFloat(16)

    @EnvironmentObject var partida: Partida

    
    var body: some View {
        LazyVGrid(columns: [
            GridItem(.flexible(minimum: 100, maximum: 200), spacing: cteSpacing),
            GridItem(.flexible(minimum: 100, maximum: 200), spacing: cteSpacing),
            GridItem(.flexible(minimum: 100, maximum: 200), spacing: 12)
        ], spacing: cteSpacing, content: {
            if let winner = partida.playerWinner {
                AwardView(award: winner)
            }
            if let loser = partida.playerLoser {
                AwardView(award: loser)
            }
            if let uphill = partida.playerWinnerUpHill {
                AwardView(award: uphill)
            }
            if let todas = partida.playerWinnerAllCards{
                AwardView(award: todas)
            }
            if let downhill = partida.playerWinnerDownHill {
                AwardView(award: downhill)
            }
        })
        .padding(.horizontal , cteSpacing)
    }
}

struct AwardsView_Previews: PreviewProvider {
    static var previews: some View {
        AwardsView()
            .environmentObject(Partida.preview_4PlayersPocheadaFullEmpat4e())
            .preferredColorScheme(.dark)
    }
}
