//
//  Ronda.swift
//  IPocha
//
//  Created by Enrique Vicent on 07/11/2020.
//

import Foundation
struct Ronda:Identifiable {
    var id: Int
    var modificadorRonda : ModificadoresRonda
    var aciertos : [UUID: Int]
    
    init(players ids:[UUID], roundNumber round:Int){
        self.id = round
        self.aciertos = ids.reduce([:], { (partial, playerId) in
            var result:[UUID:Int]=partial
            result[playerId]=0
            return result
        })
        self.modificadorRonda = .simple
    }
    
    func puntosXcartas (_ point:Int) -> Int {
        let rawPoints:Int
        if point < 0 {
            rawPoints = 5 * point
        } else {
            rawPoints = 10 + point * 5
        }
        let resultPoints:Int
        switch self.modificadorRonda {
        case .simple:
            resultPoints = rawPoints
        case .doble:
            resultPoints = 2 * rawPoints
        case .triple:
            resultPoints = 3 * rawPoints
        }
        return resultPoints
    }

    var sumario:[UUID:(aciertos:Int, puntos:Int)] {
        aciertos.reduce([:], { (acum, item) in
            var copy = acum
            copy [item.key] = (aciertos: item.value, puntos: puntosXcartas(item.value))
            return copy
        })
    }
    
    mutating func inc (player: UUID) { variatePoints(player: player, amount: 1)}
    mutating func dec (player: UUID) { variatePoints(player: player, amount: -1)}
    private mutating func variatePoints(player: UUID, amount: Int){
        if let puntosActuales = self.aciertos[player] {
            self.aciertos[player] = puntosActuales + amount
        }
    }

}



