//
//  ContentView.swift
//  IPocha
//
//  Created by Enrique Vicent on 27/10/2020.
//

import SwiftUI
import CoreData

struct ConfigView: View {
    
    //@ObservedObject var partida: Partida
    @EnvironmentObject var partida: Partida
    @EnvironmentObject var settings : Settings
    @State private var isEditable = false

    @State var newPlayer = ""
    let dummi: [Player] = [Player( name: "String")]

    var body: some View {
        VStack {
            Text("Opciones").font(.title)
            List{
                Section (header: Text("Reglas")){
                    Picker(selection: self.$partida.rules, label: Text("Tipo partida")){
                        Text("Normal").tag(Rules.pocha)
                        Text("Pocheada (extendida)").tag(Rules.pocheada)
                    }
                    .pickerStyle(SegmentedPickerStyle())
                }
                Section (header:
                            HStack {
                                Text("Jugadores (\(partida.numberOfPlayers))"
                                )
                                Spacer()
                                if self.partida.players.count>1 {
                                    Button(action: {
                                        withAnimation{
                                            isEditable = !isEditable
                                        }
                                    }){
                                        if self.isEditable {
                                            Image(systemName: "pencil.slash")
                                        } else {
                                            Image(systemName: "rectangle.and.pencil.and.ellipsis")
                                        }
                                    }
                                } else {
                                    EmptyView()
                                }
                            }){
                    ForEach(self.partida.players){ player in
                        VStack {
                            Text("\(player.name)")
                        }
                        //TextField("jugador", text: $partida.players[playerIndex])
                    }
                    .onDelete(perform: { indexSet in
                        if  let index = indexSet.first {
                            _ = self.partida.removePlayer(index: index)
                        }
                    })
                    .onMove(perform: { indices, newOffset in
                        withAnimation {
                            self.partida.players.move(fromOffsets: indices, toOffset: newOffset)
                            isEditable = false
                        }
                    })
                    .onLongPressGesture {
                        withAnimation{
                            isEditable = !isEditable
                            
                        }
                    }
                    HStack{
                        TextField("nuevo jugador..", text: $newPlayer )
                        Button(action: {
                            withAnimation {
                                _ = self.partida.addPlayer(name: newPlayer)
                                newPlayer=""
                            }
                        }) {
                            HStack {
                                Text("Añadir").font(.callout).foregroundColor(Color.blue)
                                Image(systemName: "person.crop.circle.fill.badge.plus").foregroundColor(.blue)
                            }
                        }.disabled(newPlayer.isEmpty)
                    }
                }
                Section (header: Text ("Preferencias")){
                    VStack {
                        HStack {
                            Text("Datos en el resumen")
                            Spacer()
                        }
                        Picker(selection: self.$settings.preferredData, label: Text("Vista de datos favorita")) /*@START_MENU_TOKEN@*/{
                            Text("Aciertos").tag(PreferedDataView.aciertos)
                            Text("Puntos").tag(PreferedDataView.puntos)
                            Text("Total").tag(PreferedDataView.acum)
                            Text("Clasico").tag(PreferedDataView.puntosAndAcum)
                        }.pickerStyle(SegmentedPickerStyle())
                    }
                }
                Section (header: Text("Resumen")) {
                    HStack {
                        Text("numero de cartas")
                        Spacer()
                        Text("\(partida.baraja)")
                    }
                    if !self.partida.players.isEmpty {
                        ForEach(dummi){_ in
                            HStack {
                                Spacer()
                                Text("<< Borrar Partida").foregroundColor(.red)
                            }
                        }.onDelete(perform: { _ in
                            partida.clear()
                            isEditable = false
                        })
                    } else {
                        EmptyView()
                    }
                }
            }
            .environment(\.editMode, isEditable ? .constant(.active) : .constant(.inactive))
        }
    }
}



struct ConfigView_Previews: PreviewProvider {
    static func data () -> Partida {
        let partida = Partida()
            .addPlayer(name: "pepe")
            .addPlayer(name: "lucas")
        return partida
    }
    static var previews: some View {
        Group {
            ConfigView().environmentObject(data()).environmentObject(Settings())
        }
    }
}
