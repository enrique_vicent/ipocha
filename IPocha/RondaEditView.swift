//
//  RondaEditView.swift
//  IPocha
//
//  Created by Enrique Vicent on 18/11/20.
//

import SwiftUI

struct RondaEditView: View {
    @EnvironmentObject var partida: Partida
    @EnvironmentObject var settings : Settings
    var ronda: Int
    
    func players() -> [Player] { partida.players}
    
    func dec(playerId:UUID){
        partida.points[partida.currentRound].dec(player: playerId)
    }
    
    func inc(playerId:UUID){
        partida.points[partida.currentRound].inc(player: playerId)
    }
    
    var body: some View {
        VStack {
            HStack{
                Picker("", selection: $partida.points[ronda].modificadorRonda) {
                    Text("x1").tag(ModificadoresRonda.simple)
                    Text("X2").tag(ModificadoresRonda.doble)
                    Text("X3").tag(ModificadoresRonda.triple)
                }.pickerStyle(SegmentedPickerStyle()).padding(.bottom)
            }
            ForEach(players()){ player in
                HStack {
                    Spacer()
                    Text (player.name.capitalized )
                        .fontWeight(.bold)
                        .padding(.leading)
                    Text ("\(partida.findRonda(identity: ronda).aciertos[player.id] ?? 0)")
                        .font(.caption)
                        .fontWeight(.heavy)
                    Stepper("aciertos") {
                        inc(playerId: player.id)
                    } onDecrement: {
                        dec(playerId: player.id)
                    }
                    .padding(.trailing)
                    
                    
                }
            }
        }.padding(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
        .background(Color.blue.opacity(0.2)).cornerRadius(20.0)
    }
}

struct RondaEditView_Previews: PreviewProvider {
    static let sutPartida = Partida.preview_4Players4Rounds()
    static let config = Settings()
    static var previews: some View {
        RondaEditView( ronda: 2)
            .environmentObject(sutPartida)
            .environmentObject(config)
    }
}
