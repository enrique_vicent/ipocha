//
//  PartidaHeadView.swift
//  IPocha
//
//  Created by Enrique Vicent on 09/11/2020.
//

import SwiftUI

struct PartidaHeadView: View {
    @EnvironmentObject var partida: Partida
    let firstRowGap = CGFloat(30)
    
    var body: some View {
        VStack {
            HStack(){
                Text("Ronda")
                    .padding(.leading)
                Text("\(partida.currentRound + 1)").foregroundColor(/*@START_MENU_TOKEN@*/.blue/*@END_MENU_TOKEN@*/)
                Text("Se reparte\(partida.cartas(ronda: partida.currentRound) == 1 ? "" : "n" )")
                    .padding(.leading)
                Text("\(partida.cartas(ronda: partida.currentRound))").foregroundColor(.blue)
                Text("carta\(partida.cartas(ronda: partida.currentRound) == 1 ? "" : "s" )")
                Spacer()
            }
            HStack(){
                Spacer(minLength: firstRowGap)
                Spacer()
                ForEach(partida.players){ player in
                    PartidaHeadPlayerView(
                        name: player.name.capitalized ,
                        points: partida.sumaryPoints(upToRound: partida.currentRound)[player.id] ?? 0 )
                    Spacer()
                }
                
            }
            .padding(.top)
            Divider()
        }
    }
}

struct PartidaHeadPlayerView: View {
    var name: String
    var points: Int
    
    func colorPoints () -> Color { points < 0 ? Color.red : Color.primary }
    
    var body: some View {
        VStack{
            Text( name)
            Text( "\(points)" )
                .font(/*@START_MENU_TOKEN@*/.title2/*@END_MENU_TOKEN@*/)
                .bold().foregroundColor(colorPoints())
        }
    }
}

struct PartidaHeadView_Previews: PreviewProvider {
    
    static var previews: some View {
        PartidaHeadView().environmentObject(Partida.preview_3Players3Rounds())
    }
}
