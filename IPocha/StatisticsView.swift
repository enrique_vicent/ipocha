//
//  StatisticsView.swift
//  IPocha
//
//  Created by Enrique Vicent on 7/12/20.
//

import SwiftUI

struct StatisticsView: View {
    var body: some View {
        //NavigationView {
            ScrollView {
                Section(header: Text ("Gráfico").font(/*@START_MENU_TOKEN@*/.title/*@END_MENU_TOKEN@*/)){
                    GraphLinesView().frame(width: .infinity, height: CGFloat(275))
                }
                Section(header: Text ("Premios").font(.title)){
                    AwardsView()
                }.padding(.top)
            }
        //}
    }
}

struct StatisticsView_Previews: PreviewProvider {
    static var previews: some View {
        StatisticsView().environmentObject(Partida.preview_4PlayersPocheadaFull())
        StatisticsView().environmentObject(Partida.preview_4PlayersPocheadaFullEmpat4e())
            .preferredColorScheme(.dark)
        StatisticsView().environmentObject(Partida.preview_3Players3Rounds())
        StatisticsView().environmentObject(Partida.preview_create2Players())
        StatisticsView().environmentObject(Partida())
    }
}
