//
//  RondaView.swift
//  IPocha
//
//  Created by Enrique Vicent on 14/11/20.
//

import SwiftUI

struct RondaView: View {
    @EnvironmentObject var partida: Partida
    @EnvironmentObject var settings : Settings
    var ronda: Int

    //func players() -> [Player] { partida.players}
        
    var body: some View {
        let whiteResult = VStack {
            HStack {
                HStack {
                    Text("\(partida.cartas(ronda: ronda))").frame(minWidth: 30, idealWidth: 30, maxWidth: 30, minHeight: /*@START_MENU_TOKEN@*/0/*@END_MENU_TOKEN@*/, idealHeight: 0 , maxHeight: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                    
                    ForEach(partida.players){ player in
                        HStack {
                            Spacer()
                            PointView(playerId: player.id, viewMode: self.settings.preferredData, ronda: ronda)
                            Spacer()
                        }
                    }
                }
            }.onTapGesture {
                partida.currentRound = ronda
            }
            if ronda == partida.currentRound {
                RondaEditView( ronda: ronda).animation(.easeInOut)
            } else {
                EmptyView()
            }
        }
        .padding(.vertical, 5.0)
        var bgcolor: AnyView {
            switch partida.findRonda(identity: ronda).modificadorRonda {
            case .simple:
                return AnyView(EmptyView())
            case .doble:
                return AnyView(Color("DoubleColor"))
            case .triple:
                return AnyView(Color("TripleColor"))
            }
        }
        return whiteResult.background(bgcolor).cornerRadius(20.0)
        
    }
}

struct RondaView_Previews: PreviewProvider {
    
    static func createSettings (peferedView : PreferedDataView) -> Settings {
        let result = Settings()
        result.preferredData = peferedView
        return result
    }
    static var previewRonda = Partida.preview_4Players4Rounds()
    static var previews: some View {
        Group {
            RondaView(ronda: 3)
                .environmentObject(previewRonda)
                .environmentObject(createSettings(peferedView: .aciertos))
            RondaView(ronda: 2)
                .environmentObject(previewRonda)
                .environmentObject(createSettings(peferedView: .puntos))
            RondaView(ronda: 0)
                .environmentObject(previewRonda)
                .environmentObject(createSettings(peferedView: .puntosAndAcum))
            RondaView(ronda: 1)
                .environmentObject(previewRonda)
                .environmentObject(createSettings(peferedView: .acum))
            
        }
    }
}

