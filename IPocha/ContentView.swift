//
//  ContentView.swift
//  IPocha
//
//  Created by Enrique Vicent on 09/11/2020.
//

import SwiftUI

struct ContentView: View {
    @EnvironmentObject var partida: Partida
    

    var body: some View {
        
        TabView() {
            ConfigView().environmentObject(partida).tabItem {
                Image(systemName: "slider.horizontal.3")
                Text("Configurar")
            }.tag(1)
            PlayView().environmentObject(partida).tabItem {
                Image(systemName: "highlighter")
                Text("Jugar")
            }.tag(2)
            StatisticsView().environmentObject(partida).tabItem {
                Image(systemName: "chart.pie")
                Text("Estadísticas")
            }.padding().tag(3)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
        
    static var previews: some View {
        ContentView().environmentObject(Partida.preview_4Players4Rounds()).environmentObject(Settings())
    }
}
