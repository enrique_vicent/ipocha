//
//  PartidaTestData.swift
//  IPocha
//
//  Created by Enrique Vicent on 25/11/20.
//

import Foundation

extension Partida {
    //for testing
    static func preview_4Players4Rounds () -> Partida {
        let result = Partida()
        _ = result.addPlayer(name: "antonio")
        _ = result.addPlayer(name: "pepe")
        _ = result.addPlayer(name: "ana martin")
        _ = result.addPlayer(name: "francisco")
        _ = result.appendRound()
        result.points[result.currentRound].dec(player: result.players[0].id)
        _ = result.appendRound()
        result.points[result.currentRound].modificadorRonda = .doble
        result.points[result.currentRound].dec(player: result.players[0].id)
        result.points[result.currentRound].inc(player: result.players[1].id)
        _ = result.appendRound()
        result.points[result.currentRound].dec(player: result.players[0].id)
        result.points[result.currentRound].inc(player: result.players[1].id)
        result.points[result.currentRound].inc(player: result.players[1].id)
        result.points[result.currentRound].dec(player: result.players[2].id)
        _ = result.appendRound()
        result.points[result.currentRound].dec(player: result.players[0].id)
        result.points[result.currentRound].dec(player: result.players[1].id)
        result.points[result.currentRound].dec(player: result.players[2].id)
        return result
    }

    static func preview_3Players3Rounds () -> Partida {
        let result = Partida()
        _ = result.addPlayer(name: "antonio")
        _ = result.addPlayer(name: "pepe")
        _ =  result.addPlayer(name: "ana martin")
        _ = result.appendRound()
        result.points[result.currentRound].dec(player: result.players[0].id)
        _ = result.appendRound()
        result.points[result.currentRound].modificadorRonda = .doble
        result.points[result.currentRound].dec(player: result.players[0].id)
        result.points[result.currentRound].inc(player: result.players[1].id)
        _ = result.appendRound()
        result.points[result.currentRound].dec(player: result.players[0].id)
        result.points[result.currentRound].inc(player: result.players[1].id)
        result.points[result.currentRound].inc(player: result.players[1].id)
        result.points[result.currentRound].dec(player: result.players[2].id)
        //_ = result.appendRound()
        return result
    }
    
    
    static func preview_4PlayersPochaHalf () -> Partida {
        let result = Partida()
        _ = result.addPlayer(name: "Juan")
        _ = result.addPlayer(name: "manolo")
        _ =  result.addPlayer(name: "andres")
        _ =  result.addPlayer(name: "lucas")
        _ = result.appendRound()//1 . 0
        result.points[result.currentRound].dec(player: result.players[0].id)
        _ = result.appendRound()//2 . 1
        result.points[result.currentRound].dec(player: result.players[0].id)
        result.points[result.currentRound].inc(player: result.players[1].id)
        _ = result.appendRound()//3 . 2
        result.points[result.currentRound].dec(player: result.players[0].id)
        result.points[result.currentRound].inc(player: result.players[1].id)
        result.points[result.currentRound].inc(player: result.players[1].id)
        result.points[result.currentRound].dec(player: result.players[2].id)
        _ = result.appendRound()//4 . 3
        result.points[result.currentRound].dec(player: result.players[0].id)
        _ = result.appendRound()//5 . 4
        result.points[result.currentRound].dec(player: result.players[1].id)
        result.points[result.currentRound].inc(player: result.players[0].id)
        _ = result.appendRound()//6 . 5
        result.points[result.currentRound].dec(player: result.players[1].id)
        result.points[result.currentRound].inc(player: result.players[0].id)
        result.points[result.currentRound].inc(player: result.players[2].id)
        _ = result.appendRound()//7 . 6
        result.points[result.currentRound].dec(player: result.players[2].id)
        result.points[result.currentRound].inc(player: result.players[1].id)
        result.points[result.currentRound].inc(player: result.players[1].id)
        _ = result.appendRound()//8 . 7
        result.points[result.currentRound].dec(player: result.players[3].id)
        result.points[result.currentRound].inc(player: result.players[1].id)
        result.points[result.currentRound].inc(player: result.players[1].id)
        result.points[result.currentRound].inc(player: result.players[2].id)
        _ = result.appendRound()//9 . 8
        result.points[result.currentRound].dec(player: result.players[2].id)
        result.points[result.currentRound].inc(player: result.players[1].id)
        result.points[result.currentRound].inc(player: result.players[3].id)
        _ = result.appendRound()//10 . 9
        result.points[result.currentRound].dec(player: result.players[3].id)
        _ = result.appendRound()//10 . 10
        result.points[result.currentRound].dec(player: result.players[2].id)
        result.points[result.currentRound].inc(player: result.players[1].id)
        result.points[result.currentRound].inc(player: result.players[1].id)
        result.points[result.currentRound].inc(player: result.players[3].id)
        _ = result.appendRound()//10 . 11
        result.points[result.currentRound].dec(player: result.players[2].id)
        result.points[result.currentRound].dec(player: result.players[1].id)
        result.points[result.currentRound].dec(player: result.players[1].id)
        result.points[result.currentRound].dec(player: result.players[2].id)
        _ = result.appendRound()//10 . 12
        result.points[result.currentRound].dec(player: result.players[1].id)
        result.points[result.currentRound].inc(player: result.players[0].id)
        result.points[result.currentRound].inc(player: result.players[0].id)
        result.points[result.currentRound].inc(player: result.players[0].id)
        _ = result.appendRound()//9 . 13
        result.points[result.currentRound].dec(player: result.players[1].id)
        result.points[result.currentRound].inc(player: result.players[0].id)
        result.points[result.currentRound].inc(player: result.players[2].id)
        result.points[result.currentRound].inc(player: result.players[2].id)
        return result
    }
    
    static func preview_4PlayersPocheadaFull () -> Partida {
        let result = Partida()
        result.rules = .pocheada
        _ = result.addPlayer(name: "Juan")
        _ = result.addPlayer(name: "manolo")
        _ =  result.addPlayer(name: "andres")
        _ = result.appendRound()//1-0
        result.points[result.currentRound].dec(player: result.players[0].id)
        _ = result.appendRound()//1-1
        result.points[result.currentRound].modificadorRonda = .doble
        result.points[result.currentRound].dec(player: result.players[0].id)
        result.points[result.currentRound].inc(player: result.players[1].id)
        _ = result.appendRound()//1-2
        result.points[result.currentRound].dec(player: result.players[0].id)
        result.points[result.currentRound].inc(player: result.players[1].id)
        result.points[result.currentRound].inc(player: result.players[1].id)
        result.points[result.currentRound].dec(player: result.players[2].id)
        _ =  result.addPlayer(name: "lucas")
        _ = result.appendRound()//1-3
        result.points[result.currentRound].dec(player: result.players[0].id)
        _ = result.appendRound()//2-4
        result.points[result.currentRound].dec(player: result.players[1].id)
        _ = result.appendRound()//3-5
        result.points[result.currentRound].dec(player: result.players[1].id)
        _ = result.appendRound()//4-6
        result.points[result.currentRound].dec(player: result.players[2].id)
        _ = result.appendRound()//5-7
        result.points[result.currentRound].dec(player: result.players[3].id)
        _ = result.appendRound()//6-8
        result.points[result.currentRound].dec(player: result.players[2].id)
        _ = result.appendRound()//7-9
        result.points[result.currentRound].modificadorRonda = .doble
        result.points[result.currentRound].dec(player: result.players[3].id)
        _ = result.appendRound()//8-10
        result.points[result.currentRound].dec(player: result.players[2].id)
        _ = result.appendRound()//9-11
        result.points[result.currentRound].dec(player: result.players[2].id)
        _ = result.appendRound()//10-12
        result.points[result.currentRound].dec(player: result.players[1].id)
        _ = result.appendRound()//10-13
        result.points[result.currentRound].modificadorRonda = .doble
        result.points[result.currentRound].dec(player: result.players[1].id)
        _ = result.appendRound()//10-14
        result.points[result.currentRound].dec(player: result.players[2].id)
        _ = result.appendRound()//10-15
        result.points[result.currentRound].dec(player: result.players[3].id)
        _ = result.appendRound()//9-16
        result.points[result.currentRound].dec(player: result.players[2].id)
        _ = result.appendRound()//8-17
        result.points[result.currentRound].dec(player: result.players[3].id)
        _ = result.appendRound()//7-18
        result.points[result.currentRound].dec(player: result.players[2].id)
        _ = result.appendRound()//6-19
        result.points[result.currentRound].modificadorRonda = .triple
        result.points[result.currentRound].dec(player: result.players[2].id)
        _ = result.appendRound()//5-20
        result.points[result.currentRound].dec(player: result.players[0].id)
        _ = result.appendRound()//4-21
        result.points[result.currentRound].dec(player: result.players[1].id)
        _ = result.appendRound()//3-22
        result.points[result.currentRound].dec(player: result.players[2].id)
        _ = result.appendRound()//2-23
        result.points[result.currentRound].dec(player: result.players[2].id)
        _ = result.appendRound()//10-24
        result.points[result.currentRound].dec(player: result.players[1].id)
        _ = result.appendRound()//10-25
        result.points[result.currentRound].modificadorRonda = .doble
        result.points[result.currentRound].dec(player: result.players[2].id)
        _ = result.appendRound()//10-26
        result.points[result.currentRound].dec(player: result.players[1].id)
        _ = result.appendRound()//10-27
        result.points[result.currentRound].dec(player: result.players[0].id)
        result.points[result.currentRound].dec(player: result.players[0].id)
        return result
    }
    
    static func preview_4PlayersPocheadaFullEmpat4e () -> Partida {
        let result = preview_4PlayersPocheadaFull()
        _ = result.appendRound()
        result.points[result.currentRound].dec(player: result.players[3].id)
        _ = result.appendRound()
        result.points[result.currentRound].dec(player: result.players[3].id)

        return result
    }

        
    
    static func preview_create2Players () -> Partida{
        let sut1 = Partida()
        _ = sut1.addPlayer(name: "antonio")
        return sut1.addPlayer(name: "pepe")
    }

}
