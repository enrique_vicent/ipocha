//
//  Settings.swift
//  IPocha
//
//  Created by Enrique Vicent on 14/11/20.
//

import Foundation

enum PreferedDataView {
    case aciertos
    case puntos
    case acum
    case puntosAndAcum
}

class Settings :ObservableObject {
    @Published var preferredData: PreferedDataView = .aciertos
}
