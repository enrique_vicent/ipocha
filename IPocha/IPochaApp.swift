//
//  IPochaApp.swift
//  IPocha
//
//  Created by Enrique Vicent on 27/10/2020.
//

import SwiftUI

@main
struct IPochaApp: App {
    var partida = Partida()
    var settings = Settings()
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environmentObject(partida)
                .environmentObject(settings)
        }
    }
}

struct IPochaApp_Previews: PreviewProvider {
    static var previews: some View {
        /*@START_MENU_TOKEN@*/Text("Hello, World!")/*@END_MENU_TOKEN@*/
    }
}
