//
//  GraphLinesView.swift
//  IPocha
//
//  Created by Enrique Vicent on 29/11/20.
//

import SwiftUI

struct GraphLinesView: View {
    
    @EnvironmentObject var partida: Partida
    
    static private let colorList: [Color] = [
        .red,
        .blue,
        .green,
        .orange,
        .yellow,
        .pink,
        .purple,
    ]
    
    static private func getColor (player index: Int) -> Color {
        return colorList[index % colorList.count]
    }
    
    private func getColor (playerUUID pl: UUID) -> Color {
        if let index = partida.playerIndex (playerUUID: pl) {
            return GraphLinesView.colorList[index % GraphLinesView.colorList.count]
        } else {
            return Color.black
        }
    }
    
    
    private func zeroY ( height: CGFloat, data: PartidaGraph) -> CGFloat {
        let rangePoints = data.maxScore - data.minScore
        let ratioPixelsPerPoint: CGFloat = height / CGFloat(rangePoints)
        let normalizedPoints = 0 - data.minScore
        return height - ratioPixelsPerPoint * CGFloat(normalizedPoints)
    }
    
    var body: some View {
        let data = partida.graphData
        return VStack {
            if data.rounds.count > 2 && partida.players.count > 0 {
                GeometryReader { geometry -> AnyView in

                    return AnyView( ZStack{
                        GraphLineView(data: PartidaGraph(maxScore: data.maxScore, minScore: data.minScore, span: 2, score: [[0],[0]], rounds: []),
                                      playerIndex: 0,
                                      color: .gray)
                        ForEach(partida.players){ player in
                            if let playerIndex = partida.playerIndex(playerUUID: player.id){
                                GraphLineView(data: data,
                                              playerIndex: playerIndex,
                                              color: GraphLinesView.getColor(player: playerIndex)
                                )
                            } else {
                                EmptyView()
                            }
                        }
                    }.frame(minWidth: 0,
                            maxWidth: .infinity,
                            minHeight: 0,
                            maxHeight: .infinity,
                            alignment: .center)
                    .padding()
                    )
                }
            } else {
                Text("Insuficientes datos")
            }
            HStack {
                Spacer()
                ForEach(partida.players){ player in
                    Text("\(player.name)")
                        .foregroundColor(self.getColor(playerUUID: player.id))
                    Spacer()
                }
            }
        }
    }
}

struct GraphLinesView_Previews: PreviewProvider {
    static var previews: some View {
        GraphLinesView().environmentObject(Partida.preview_4PlayersPocheadaFull())
        GraphLinesView().environmentObject(Partida.preview_4PlayersPocheadaFullEmpat4e())
            .preferredColorScheme(.dark)
        GraphLinesView().environmentObject(Partida.preview_3Players3Rounds())
        GraphLinesView().environmentObject(Partida.preview_create2Players())
        GraphLinesView().environmentObject(Partida())
    }
}
