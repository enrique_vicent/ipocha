//
//  GraphLineView.swift
//  IPocha
//
//  Created by Enrique Vicent on 29/11/20.
//

import SwiftUI

struct GraphLineView: View {
    static let lineWidth: CGFloat = 2.5
    var data: PartidaGraph
    var playerIndex: Int
    let color: Color
        
    private func scaleY ( points : Int, height: CGFloat) -> CGFloat {
        let rangePoints = data.maxScore - data.minScore
        let ratioPixelsPerPoint: CGFloat = height / CGFloat(rangePoints)
        let normalizedPoints = points - data.minScore
        return height - ratioPixelsPerPoint * CGFloat(normalizedPoints)
    }
    
    private func scaleX ( ronda : Int, width: CGFloat ) -> CGFloat {
        let pixelsPerRound = width / CGFloat(data.span - 1 )
        return pixelsPerRound * CGFloat(ronda)
    }
    
    
    func graphPoint(ronda:Int, points:Int, height: CGFloat, width: CGFloat ) -> CGPoint {
        return CGPoint(
            x: scaleX(ronda: ronda, width: width),
            y: scaleY(points: points, height: height)
        )
    }
    
    var body: some View {
        GeometryReader{ geometry in
            Path { path in
                let puntos = data.score.map {$0[playerIndex]}
                var isFirstPoint = true
                for pointIndx in 0 ..< data.span {
                    if let ptx = puntos[pointIndx] {
                        let pixel: CGPoint = graphPoint(ronda: pointIndx, points: ptx, height: geometry.size.height, width: geometry.size.width)
                        if isFirstPoint{
                            path.move(to: pixel)
                            isFirstPoint = false
                        } else {
                            path.addLine(to: pixel)
                        }
                    }
                }
            }.stroke(lineWidth: GraphLineView.lineWidth).fill(color)


        }
    }
}



struct GraphLineView_Previews: PreviewProvider {
    static var previews: some View {
        GraphLineView(data: Partida.preview_4PlayersPocheadaFull().graphData, playerIndex: 0, color: Color.red)
        GraphLineView(data: Partida.preview_4PlayersPocheadaFullEmpat4e().graphData, playerIndex: 3, color: Color.green)
        GraphLineView(data: Partida.preview_3Players3Rounds().graphData, playerIndex: 0, color: Color.gray)
        GraphLineView(data: Partida.preview_create2Players().graphData, playerIndex: 0, color: Color.blue)
        GraphLineView(data: Partida().graphData, playerIndex: 0, color: Color.orange)
    }
}
