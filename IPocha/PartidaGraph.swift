//
//  PartidaGraph.swift
//  IPocha
//
//  Created by Enrique Vicent on 27/11/20.
//

import Foundation

struct PartidaGraph {
    let maxScore: Int
    let minScore: Int
    let span: Int
    let score: [[Int?]]
    let rounds: [Int]
}

extension PartidaGraph {
    init ( _ partida : Partida){
        self.span = max (partida.gameSpan.current, partida.gameSpan.planned)

        let playerIDs = partida.players.map(){$0.id}
        var score: [[Int?]] = [[Int?]](repeating: [Int?](repeating:nil, count: playerIDs.count), count:span )
        var pmax = 0
        var pmin = 0
        for rnd in 0..<partida.gameSpan.current {
            let sumario = partida.sumaryPoints(upToRound: rnd)
            for ply in 0 ..< playerIDs.count {
                let plyId = playerIDs[ply]
                let point : Int? = sumario[plyId]
                score[rnd][ply] = point
                if let ppoint = point {
                    pmax = max(pmax,ppoint)
                    pmin = min(pmin,ppoint)
                }
                //print("\(point ?? 1):")
            }
        }
        for rnd in partida.gameSpan.current..<span {
            for ply in 0 ..< playerIDs.count {
                score[rnd][ply] = nil
                //print("nil")
            }
        }
        self.maxScore = pmax
        self.minScore = pmin
        self.score = score
        self.rounds = (0..<span).map(){partida.cartas(ronda: $0)}
        //for r in rounds { print ("**\(r)")}
    }
}

extension Partida{
    var graphData:  PartidaGraph  {PartidaGraph(self)}
    
    func playerIndex(playerUUID id : UUID) -> Int? {
        self.players.firstIndex(where: { (pla) -> Bool in
           pla.id == id
       })
    }
}
