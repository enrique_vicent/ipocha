//
//  RondaTests.swift
//  IPochaTests
//
//  Created by Enrique Vicent on 07/11/2020.
//

import XCTest
@testable import IPocha

class RondaTests: XCTestCase {

    let a = UUID()
    let b = UUID()
    let c = UUID()
        
    override func setUpWithError() throws {
        
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testPuntosXcartas() throws {
        var ronda = Ronda(players: [a,b,c], roundNumber: 1)
        XCTAssertEqual(ronda.puntosXcartas(3),3*5+10)
        XCTAssertEqual(ronda.puntosXcartas(0),10)
        XCTAssertEqual(ronda.puntosXcartas(1),15)
        XCTAssertEqual(ronda.puntosXcartas(2),2*5+10)
        XCTAssertEqual(ronda.puntosXcartas(4),4*5+10)
        XCTAssertEqual(ronda.puntosXcartas(-1),-5)
        XCTAssertEqual(ronda.puntosXcartas(-2),-10)
        ronda.modificadorRonda = .doble
        XCTAssertEqual(ronda.puntosXcartas(3),2*(3*5+10))
        XCTAssertEqual(ronda.puntosXcartas(0),20)
        XCTAssertEqual(ronda.puntosXcartas(-2),-20)
        ronda.modificadorRonda = .triple
        XCTAssertEqual(ronda.puntosXcartas(3),3*(3*5+10))
        XCTAssertEqual(ronda.puntosXcartas(0),30)
        XCTAssertEqual(ronda.puntosXcartas(-2),-30)
    }
    
    func testPuntosResumen() throws {
        var ronda = Ronda(players: [a,b,c], roundNumber: 1)
        ronda.aciertos[b] = 2
        ronda.aciertos[c] = -1
        ronda.modificadorRonda = .doble
        var expected : [UUID:(aciertos:Int, puntos:Int)] = [:]
        expected[a]=(aciertos:0, puntos:20)
        expected[b]=(aciertos:2,puntos:40)
        expected[c]=(aciertos:-1,puntos:-10)
        let results = ronda.sumario
        XCTAssertEqual(results.count , expected.count)
        XCTAssertEqual(results[a]?.puntos, expected[a]?.puntos)
        XCTAssertEqual(results[b]?.puntos, expected[b]?.puntos)
        XCTAssertEqual(results[c]?.puntos, expected[c]?.puntos)
        XCTAssertEqual(results[a]?.aciertos, expected[a]?.aciertos)
        XCTAssertEqual(results[b]?.aciertos, expected[b]?.aciertos)
        XCTAssertEqual(results[c]?.aciertos, expected[c]?.aciertos)
    }

    func testPuntosIncDec() throws {
        var ronda = Ronda(players: [a,b,c], roundNumber: 1)
        ronda.aciertos[b] = 2
        ronda.aciertos[c] = -1
        ronda.dec(player: a)
        ronda.inc(player: c)
        XCTAssertEqual(ronda.aciertos[a], -1)
        XCTAssertEqual(ronda.aciertos[b], 2)
        XCTAssertEqual(ronda.aciertos[c], 0)
    }

}
