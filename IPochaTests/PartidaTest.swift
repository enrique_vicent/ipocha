//
//  PartidaTest.swift
//  IPochaTests
//
//  Created by Enrique Vicent on 28/10/2020.
//

import XCTest
@testable import IPocha

class PartidaTest: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    

    func testAddPlayer() throws {
        let sut1 = Partida()
        XCTAssertEqual(sut1.numberOfPlayers,0)
        let sut = sut1.addPlayer(name: "antonio")
        XCTAssertEqual(sut.numberOfPlayers,1)
        sut.players=[Player(name: "pepe"), Player(name:"marco")]
        XCTAssertEqual(sut.numberOfPlayers,2)
        sut.players.append(Player(name:"lucas"))
        XCTAssertEqual(sut.numberOfPlayers,3)
        
    }
    
    func testRemovePlayer() throws {
        let sut = Partida.preview_create2Players()
        let result = sut.removePlayer(index: 1)
        XCTAssertEqual(result.numberOfPlayers,1)
    }
    
    func testCreatePartida() throws {
        let sut = Partida.preview_create2Players()
        XCTAssertFalse(sut.isActive)
        _ = sut.appendRound()
        XCTAssertTrue(sut.isActive)
    }
    
    func testGetAciertos() throws {
        let partida = Partida.preview_3Players3Rounds()
        let playerA = partida.players[0].id
        let playerB = partida.players[1].id
        XCTAssertEqual(partida.points[0].aciertos[playerA] , -1)
        XCTAssertEqual(partida.points[1].aciertos[playerA] , -1)
        XCTAssertEqual(partida.points[2].aciertos[playerA] , -1)
        XCTAssertEqual(partida.points[0].aciertos[playerB] , 0)
        XCTAssertEqual(partida.points[1].aciertos[playerB] , 1)
        XCTAssertEqual(partida.points[2].aciertos[playerB] , 2)
    }
    
    func testGetPuntos() throws {
        let partidavacia = Partida.preview_create2Players()
        let puntosVacio = partidavacia.sumaryPoints(upToRound: 0)
        let player0 = partidavacia.players[0].id
        XCTAssertEqual(puntosVacio[player0], 0)
        
        let partida = Partida.preview_3Players3Rounds()
        let playerA = partida.players[0].id
        let playerB = partida.players[1].id
        let playerC = partida.players[2].id
        let puntosR1 = partida.sumaryPoints(upToRound: 0)
        let puntosR3 = partida.sumaryPoints(upToRound: partida.currentRound)
        let puntosR3Last1 = partida.partialPoints(fromRound: 2, toRound: partida.currentRound)
        let puntosR3Last2 = partida.partialPoints(fromRound: 1, toRound: partida.currentRound)
        let puntosR3Outside = partida.partialPoints(fromRound: 10, toRound: 11)
        
        XCTAssertEqual(puntosR1[playerA], -5)
        XCTAssertEqual(puntosR1[playerB], 10)
        XCTAssertEqual(puntosR1[playerC], 10)
        XCTAssertEqual(puntosR3[playerA], -20)
        XCTAssertEqual(puntosR3[playerB], 10 + 30 + 20)
        XCTAssertEqual(puntosR3[playerC], 10 + 20 - 5)
        XCTAssertEqual(puntosR3Last2[playerC], 20 - 5)
        XCTAssertEqual(puntosR3Last1[playerC], -5)
        XCTAssertEqual(puntosR3Outside[playerC], 0)
        
    }
    
    func testBaraja() throws {
        let partidaVacia = Partida()
        XCTAssertEqual( partidaVacia.baraja, 40)
        let partida1 = partidaVacia.addPlayer(name: "")
        XCTAssertEqual( partida1.baraja, 40)
        let partida2 = partida1.addPlayer(name: "")
        XCTAssertEqual( partida2.baraja, 40)
        let partida3 = partida2.addPlayer(name: "")
        XCTAssertEqual( partida3.baraja, 40-13)
        let partida4 = partida3.addPlayer(name: "")
        XCTAssertEqual( partida4.baraja, 40)
        let partida5 = partida4.addPlayer(name: "")
        XCTAssertEqual( partida5.baraja, 40)
        let partida6 = partida5.addPlayer(name: "")
        XCTAssertEqual( partida6.baraja, 36)
        let partida7 = partida6.addPlayer(name: "")
        XCTAssertEqual( partida7.baraja, 35)
        let partida8 = partida7.addPlayer(name: "")
        XCTAssertEqual( partida8.baraja, 40)
        let partida9 = partida8.addPlayer(name: "")
        XCTAssertEqual( partida9.baraja, 36)
    }
    
    func testCartasRonda() throws {
        let partida3 = Partida.preview_3Players3Rounds()
        let partida4 = Partida.preview_3Players3Rounds().addPlayer(name: "jacinto")
        let pocheada4 = Partida.preview_3Players3Rounds().addPlayer(name: "jacinto")
        pocheada4.rules = .pocheada
        let partida5 = Partida.preview_3Players3Rounds().addPlayer(name: "jacinto").addPlayer(name: "flinch")
        
        let expected3 = [1,2,3,4,5,6,7,8,9,9,9,8,7,6,5,4,3,2,1,0]
        let expected4 = [1,2,3,4,5,6,7,8,9,10,10,10,10,9,8,7,6,5,4,3,2,1,0]
        let expectedp4 = [1,1,1,1,2,3,4,5,6,7,8,9,10,10,10,10,9,8,7,6,5,4,3,2,10,10,10,10,0]
        let expected5 = [1,2,3,4,5,6,7,8,8,8,8,8,7,6,5,4,3,2,1,0]
        
        let testSuite = [
            (partida3, expected3, "pocha3"),
            (partida4, expected4,"pocha4"),
            (partida5, expected5, "pocha5"),
            (pocheada4, expectedp4, "pocheada4")]
        
        for (partida, expected, scenario) in testSuite {
            for i in 0 ..< expected.count {
                //print(partida.cartas(ronda: i))
                XCTAssertEqual(partida.cartas(ronda: i), expected[i], "failed scenario \(scenario) expected \(expected[i]) but found \(partida.cartas(ronda: i)) at round \(i+1)º")
            }
            
        }
        
    }
    func testTotalGrapRondas() throws {
        let partida3 = Partida.preview_3Players3Rounds().gameSpan
        XCTAssertEqual(partida3.current, 3)
        XCTAssertEqual(partida3.planned, 8*2+3)
        let partida2 = Partida.preview_create2Players().gameSpan
        XCTAssertEqual(partida2.current, 0)
        XCTAssertEqual(partida2.planned, 40)
        let partida4 = Partida.preview_4Players4Rounds().gameSpan
        XCTAssertEqual(partida4.current,4)
        XCTAssertEqual(partida4.planned,9*2+4)
        let partidaPocheada4 = Partida.preview_4PlayersPocheadaFull().gameSpan
        XCTAssertEqual(partidaPocheada4.current, 28 )
        XCTAssertEqual(partidaPocheada4.planned, 28 )
        let partidaLarga = Partida.preview_4PlayersPocheadaFullEmpat4e().gameSpan
        XCTAssertEqual(partidaLarga.current, 30 )
        XCTAssertEqual(partidaLarga.planned, 3 + 9*2 + 4*2 - 1 )
        XCTAssertEqual(max(partidaLarga.current, partidaLarga.planned), 30)
    }
    



}
