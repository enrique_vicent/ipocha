//
//  PartidaGraphTest.swift
//  IPochaTests
//
//  Created by Enrique Vicent on 27/11/20.
//

import XCTest
@testable import IPocha


class PartidaGraphTest: XCTestCase {


    func testPartida3() throws {
        let partida = Partida.preview_3Players3Rounds()
        let expected = PartidaGraph(
            maxScore: 60,
            minScore: -20,
            span: partida.gameSpan.planned,
            score: [[nil]],
            rounds: []
        )
        validateSenario(partida.graphData, expected)
    }
    
    func testPartida2() throws {
        let partida = Partida.preview_create2Players()
        let expected = PartidaGraph(
            maxScore: 0,
            minScore: 0,
            span: partida.gameSpan.planned,
            score: [[nil]],
            rounds: []
        )
        validateSenario(partida.graphData, expected)

    }
    
    func testPartida4() throws {
        let partida = Partida.preview_4Players4Rounds()
        let expected = PartidaGraph(
            maxScore: 60,
            minScore: -25,
            span: partida.gameSpan.planned,
            score: [[nil]],
            rounds: []
        )
        validateSenario(partida.graphData, expected)

    }
    
    func testPartidaPocheada4() throws {
        let partida = Partida.preview_4PlayersPocheadaFull()
        let expected = PartidaGraph(
            maxScore: 240,
            minScore: -25,
            span: partida.gameSpan.planned,
            score: [[nil]],
            rounds: []
        )
        validateSenario(partida.graphData, expected)

    }
    
    func testPartidaLarga() throws {
        let partida = Partida.preview_4PlayersPocheadaFullEmpat4e()
        let expected = PartidaGraph(
            maxScore: 260,
            minScore: -25,
            span: partida.gameSpan.current,
            score: [[nil]],
            rounds: []
        )
        validateSenario(partida.graphData, expected)

    }
    
    func validateSenario ( _ a : PartidaGraph, _ b : PartidaGraph){
        XCTAssertEqual(a.span , b.span)
        XCTAssertEqual(a.maxScore, b.maxScore)
        XCTAssertEqual(a.minScore, b.minScore)
    }
    
    func testNullNonPlayed() throws {
        let partida = Partida.preview_3Players3Rounds().graphData
        XCTAssertNil(partida.score[3][0])
        XCTAssertNotNil(partida.score[2][0])
    }
    
    func testEmpty() throws {
        let partida = Partida().graphData
        XCTAssertEqual(partida.score.count, 0)
    }
}
