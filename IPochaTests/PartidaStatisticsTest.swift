//
//  PartidaStatisticsTest.swift
//  IPochaTests
//
//  Created by Enrique Vicent on 5/12/20.
//

import XCTest
@testable import IPocha

class PartidaStatisticsTest: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testStatisticsWinner() throws {
        let partida3 = Partida.preview_3Players3Rounds()
        XCTAssertEqual(partida3.playerWinner!.winner.name, partida3.players[1].name)
        XCTAssertNil( Partida.preview_create2Players().playerWinner)
        XCTAssertNil( Partida().playerWinner)
    }
    
    func testStatisticsLoser() throws {
        let partida3 = Partida.preview_3Players3Rounds()
        XCTAssertEqual(partida3.playerLoser!.winner.name, partida3.players[0].name)
    }
    
    func testStatisticsUpHill() throws {
        XCTAssertNil(Partida().playerWinnerUpHill)
        XCTAssertNil(Partida.preview_create2Players().playerWinnerUpHill)
        let partida3 = Partida.preview_3Players3Rounds()
        XCTAssertNil(partida3.playerWinnerUpHill)
        for _ in 3..<7 {
            _ = partida3.appendRound()
            XCTAssertNil(partida3.playerWinnerUpHill)
        }
        XCTAssertEqual(partida3.cartas(ronda: partida3.currentRound), 7)
        _ = partida3.appendRound()
        XCTAssertEqual(partida3.cartas(ronda: partida3.currentRound), 8)
        let soc = partida3.playerWinnerUpHill
        XCTAssertNotNil(soc)
        if let nnsoc = soc {
            XCTAssertEqual(nnsoc.winner.name, "pepe")
            XCTAssertEqual(nnsoc.amount, 110)
            XCTAssertEqual(nnsoc.amount, partida3.sumaryPoints(upToRound: partida3.currentRound)[partida3.players[1].id])
        }
        XCTAssertNil(partida3.playerWinnerAllCards)
        _ = partida3.appendRound()
        XCTAssertNotNil(partida3.playerWinnerAllCards)
        _ = partida3.appendRound()
        _ = partida3.appendRound()
        XCTAssertNil(partida3.playerWinnerDownHill)
        XCTAssertEqual(partida3.cartas(ronda: partida3.currentRound), 9)
        _ = partida3.appendRound()
        partida3.points[partida3.currentRound].inc(player: partida3.players[0].id)
        partida3.points[partida3.currentRound].inc(player: partida3.players[0].id)
        XCTAssertEqual(partida3.cartas(ronda: partida3.currentRound), 8)
        XCTAssertNotNil(partida3.playerWinnerDownHill)
        if let nnsoc = partida3.playerWinnerDownHill {
            XCTAssertEqual(nnsoc.winner.name, "antonio")
            XCTAssertEqual(nnsoc.amount, 20)
        }
    }
    
    fileprivate func checkAllCardsAward(partida: Partida, winnerName:String, amount:Int, scenario:String="") {
        if let partida4hAward: Award = partida.playerWinnerAllCards{
            XCTAssertEqual(partida4hAward.winner.name, winnerName, scenario)
            XCTAssertEqual(partida4hAward.amount, amount, scenario)
        } else {
            XCTFail("scenario \(scenario) failed, because there is no award")
        }
    }
    
    func testStatisticsAllCards () throws {
        XCTAssertNil(Partida.preview_create2Players().playerWinnerAllCards)
        XCTAssertNil(Partida().playerWinnerAllCards)
        checkAllCardsAward(partida: Partida.preview_4PlayersPochaHalf(), winnerName: "Juan", amount: 10+10+10+25, scenario: "4 player Half")
        checkAllCardsAward(partida: Partida.preview_4PlayersPocheadaFull(), winnerName: "lucas", amount: 40-5+50, scenario: "4 player pocheada Full")
        checkAllCardsAward(partida: Partida.preview_4PlayersPocheadaFullEmpat4e() , winnerName: "Juan", amount: 100, scenario: "4 player pocheada Full + 2")
    }
    
    func testStatisticsDownHillExcludesDraws () throws {
        let partida = Partida.preview_3Players3Rounds()
        for _ in 3 ... partida.rondafinBajada {
            _ = partida.appendRound()
        }
        XCTAssertEqual(partida.cartas(ronda: partida.currentRound), 1)
        let expectedValue = partida.playerWinnerDownHill!.amount
        _ = partida.appendRound()
        XCTAssertEqual(partida.playerWinnerDownHill!.amount, expectedValue)

        let expectedValuePocheadas = 85
        XCTAssertEqual(Partida.preview_4PlayersPocheadaFull().playerWinnerDownHill!.amount, expectedValuePocheadas)
        XCTAssertEqual(Partida.preview_4PlayersPocheadaFullEmpat4e().playerWinnerDownHill!.amount, expectedValuePocheadas)
    }

}
